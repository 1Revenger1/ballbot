# Blinkin LED - PWM Control
# 16 channel PWM map - adafruit servokit
# Acceleramotor BNO-055 - Adafruit
# Bluetooth - Bluetooth Friend - Send characters over Serial

import board
import busio
import adafruit_bno055
import adafruit_pca9685
from threading import Timer

def pwmFrequency = 333


class MotorController:
    def __init__(self, pwmHat) {
        self.motor1Channel = pwmHat.channels[0]
        self.motor2Channel = pwmHat.channels[1]
        self.motor2Channel = pwmHat.channels[2]
    }

    def setMotors(self, duty1, duty2, duty3) {
        self.motor1Channel.duty_cycle = duty1
        self.motor2Channel.duty_cycle = duty2
        self.motor3Channel.duty_cycle = duty3
    }

    def stopMotors(self) {
        self.motor1Channel.duty_cycle = 0
        self.motor2Channel.duty_cycle = 0
        self.motor3Channel.duty_cycle = 0
    }

class BallBot:
    def __init__(self) {
        i2c = busio.I2C(board.SCL, board.SDA)
        pwmHat = adafruit_pca9685.PCA9685(i2c)
        
        pwmHat.frequency = pwmFrequency
        self.pwmHat = pwmHat
        
        self.MotorController = MotorController(pwmHat)
    }

def ballBot = BallBot()
def dutyCycle = 1500
def decr = False

class RepeatedTimer:
    def __init__(self, interval, function):
        self.interval = interval
        self.is_running = False
        self._timer = None
        self.function = function


    def _run():
        self.is_running = False
        self.start()
        self


while True {

    if(dutyCycle < 2000 and not decr )
        dutyCycle += 10
    else if(dutyCycle > 2000 and not decr)
        decr = True

    if(dutyCycle > 1000 and decr) 
        dutyCycle -= 10
    else if (dutyCycle < 1000 and decr)
        decr = False

    ballBot.MotorController.setMotors(dutyCycle, dutyCycle, dutyCycle)

}

